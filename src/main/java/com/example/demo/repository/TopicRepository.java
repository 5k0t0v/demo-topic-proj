package com.example.demo.repository;

import com.example.demo.entity.Topic;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.CrudRepository;

public interface TopicRepository extends CrudRepository<Topic, Integer> {
    Slice<Topic> findAll(Pageable pageable);
}
