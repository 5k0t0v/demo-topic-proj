package com.example.demo.repository;

import com.example.demo.entity.TopicItem;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface TopicItemRepository extends CrudRepository<TopicItem, Integer> {
    Slice<TopicItem> findAll(Pageable pageable);
    int countTopicItemByTopicId(int topicId);
    ArrayList<TopicItem> findAllByTopicId (int topicId);
}
