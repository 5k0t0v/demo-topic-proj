package com.example.demo.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "topic_items")
public class TopicItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private int id;

    private String name;

    @Column(name = "order_topic")
    private int order;

    @ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;
}
