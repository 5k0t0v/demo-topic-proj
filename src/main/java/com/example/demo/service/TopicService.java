package com.example.demo.service;

import com.example.demo.entity.Topic;
import com.example.demo.entity.TopicItem;
import com.example.demo.repository.TopicItemRepository;
import com.example.demo.repository.TopicRepository;
import com.example.demo.request.TopicForm;
import com.example.demo.response.ResultResponse;
import com.example.demo.response.TopicResponse;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class TopicService {
    private final TopicRepository topicRepository;
    private final TopicItemRepository topicItemRepository;


    public TopicService(TopicRepository topicRepository, TopicItemRepository topicItemRepository) {
        this.topicRepository = topicRepository;
        this.topicItemRepository = topicItemRepository;
    }

    public ResponseEntity<ArrayList<TopicResponse>> getTopics(int offset, int limit) {
        Slice<Topic> sliceTopic = topicRepository.findAll(PageRequest.of(offset, limit));
        if (sliceTopic.isEmpty()) {
            return null;
        }
        ArrayList<TopicResponse> topicResponses = new ArrayList<>();
        for (Topic topic : sliceTopic) {
            TopicResponse topicResponse = new TopicResponse();
            topicResponse.setId(topic.getId());
            topicResponse.setName(topic.getName());
            topicResponse.setDescription(topic.getDescription());
            topicResponses.add(topicResponse);
        }

        return ResponseEntity.ok(topicResponses);
    }

    public ResponseEntity<Topic> getTopicById(int id) {
        Topic topic = topicRepository.findById(id).orElseThrow(() -> new RuntimeException("NoSuchElementException"));
        return ResponseEntity.ok(topic);
    }

    public ResponseEntity<ResultResponse> addTopic(TopicForm form) {
        if (!check(form)) {
            return ResponseEntity.ok(new ResultResponse(false));
        }

        Topic topic = new Topic();
        topic.setName(form.getName());
        topic.setDescription(form.getDescription());
        topicRepository.save(topic);

        if (!form.getListTopicItems().isEmpty()){
            addTopicItems(topic, form.getListTopicItems());
        }
        return ResponseEntity.ok(new ResultResponse(true));
    }

    private void addTopicItems(Topic topic, String listTopicItems) {
        String[] fragments = listTopicItems.split(";");
        int order = 1;
        for (String topicItemFragment : fragments){
            TopicItem topicItem = new TopicItem();
            topicItem.setTopic(topic);
            topicItem.setName(topicItemFragment.trim());
            topicItem.setOrder(order++);
            topicItemRepository.save(topicItem);
        }
    }

    private boolean check(TopicForm form) {
        if (form.getName().isEmpty()) return false;
        return !form.getDescription().isEmpty();
    }

    public ResponseEntity<ResultResponse> editTopic(int id, TopicForm form) {
        if (!check(form)) {
            return ResponseEntity.ok(new ResultResponse(false));
        }
        Topic topic = topicRepository.findById(id).orElseThrow(() -> new RuntimeException("NoSuchElementException"));
        topic.setName(form.getName());
        topic.setDescription(form.getDescription());
        topicRepository.save(topic);
        return ResponseEntity.ok(new ResultResponse(true));
    }

    public ResponseEntity<ResultResponse> deleteTopic(int id) {
        Topic topic = topicRepository.findById(id).orElseThrow(() -> new RuntimeException("NoSuchElementException"));
        topicRepository.delete(topic);
        return ResponseEntity.ok(new ResultResponse(true));
    }
}
