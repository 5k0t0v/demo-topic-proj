package com.example.demo.service;

import com.example.demo.entity.Topic;
import com.example.demo.entity.TopicItem;
import com.example.demo.repository.TopicItemRepository;
import com.example.demo.repository.TopicRepository;
import com.example.demo.request.TopicItemForm;
import com.example.demo.response.ResultResponse;
import com.example.demo.response.TopicItemResponse;
import javassist.NotFoundException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class TopicItemService {
    private final TopicItemRepository topicItemRepository;
    private final TopicRepository topicRepository;

    public TopicItemService(TopicItemRepository topicItemRepository, TopicRepository topicRepository) {
        this.topicItemRepository = topicItemRepository;
        this.topicRepository = topicRepository;
    }

    public ResponseEntity<ArrayList<TopicItemResponse>> getTopicItems(int offset, int limit) {
        Slice<TopicItem> sliceTopicItem = topicItemRepository.findAll(PageRequest.of(offset, limit));
        if (sliceTopicItem.isEmpty()) {
            return null;
        }
        ArrayList<TopicItemResponse> topicItemResponses = new ArrayList<>();
        for (TopicItem topicItem : sliceTopicItem) {
            TopicItemResponse topicItemResponse = new TopicItemResponse();
            topicItemResponse.setId(topicItem.getId());
            topicItemResponse.setName(topicItem.getName());
            topicItemResponse.setOrder(topicItem.getOrder());

            topicItemResponse.setTopic(topicItem.getTopic());

            topicItemResponses.add(topicItemResponse);
        }

        return ResponseEntity.ok(topicItemResponses);
    }

    public ResponseEntity<TopicItem> getTopicItemById(int id) {
        return ResponseEntity.ok(topicItemRepository.findById(id).orElseThrow());
    }

    public ResponseEntity<ResultResponse> addTopicItem(TopicItemForm form) throws NotFoundException {
        if (!check(form)) {
            return ResponseEntity.ok(new ResultResponse(false));
        }

        int topicId = Integer.parseInt(form.getTopicId());
        Topic topic = topicRepository.findById(topicId).orElseThrow(() -> new NotFoundException("topic "
                + form.getTopicId() + " not found"));

        TopicItem topicItem = new TopicItem();
        topicItem.setName(form.getName());

        int order;
        int countTopicItemsByTopic = topicItemRepository.countTopicItemByTopicId(topic.getId());
        if ((form.getOrder() == null) || (form.getOrder().isEmpty())) {
            order = countTopicItemsByTopic + 1;
        } else {
            order = Integer.parseInt(form.getOrder());
            if (order < countTopicItemsByTopic) {
                changeOrderField(order, topic, 1);
            }
        }
        topicItem.setOrder(order);
        topicItem.setTopic(topic);
        topicItemRepository.save(topicItem);
        return ResponseEntity.ok(new ResultResponse(true));
    }

    private void changeOrderField(int order, Topic topic, int value) {
        ArrayList<TopicItem> topicItemsList = topicItemRepository.findAllByTopicId(topic.getId());
        for (TopicItem topicItem : topicItemsList) {
            if (topicItem.getOrder() < order) continue;
            topicItem.setOrder(topicItem.getOrder() + value);
            topicItemRepository.save(topicItem);
        }
    }

    private boolean check(TopicItemForm form) {
        if (form.getName().isEmpty()) return false;
        return !form.getTopicId().isEmpty();
    }

    public ResponseEntity<ResultResponse> editTopicItem(int id, TopicItemForm form) throws NotFoundException {
        if (!check(form)) {
            return ResponseEntity.ok(new ResultResponse(false));
        }

        TopicItem topicItem = topicItemRepository.findById(id).orElseThrow(() -> new NotFoundException("topicItem "
                + form.getTopicId() + " not found"));

        topicItem.setName(form.getName());
        int topicId = Integer.parseInt(form.getTopicId());
        Topic topic = topicRepository.findById(topicId).orElseThrow(() -> new NotFoundException("topic "
                + form.getTopicId() + " not found"));

        int order;
        int countTopicItemsByTopic = topicItemRepository.countTopicItemByTopicId(topic.getId());
        if (form.getOrder() != null) {
            order = Integer.parseInt(form.getOrder());
            if (order < countTopicItemsByTopic) {
                changeOrderField(order, topic, 1);
            }
            topicItem.setOrder(order);
        }

        topicItem.setTopic(topic);
        topicItemRepository.save(topicItem);
        return ResponseEntity.ok(new ResultResponse(true));
    }

    public ResponseEntity<ResultResponse> deleteTopicItem(int id) throws NotFoundException {
        TopicItem topicItem = topicItemRepository.findById(id).orElseThrow(() -> new NotFoundException("topicItem "
                + id + " not found"));
        topicItemRepository.delete(topicItem);
        int order = topicItem.getOrder();
        int countTopicItemsByTopic = topicItemRepository.countTopicItemByTopicId(topicItem.getTopic().getId());
        Topic topic = topicItem.getTopic();
        if (order < countTopicItemsByTopic) {
            changeOrderField(order, topic, -1);
        }
        return ResponseEntity.ok(new ResultResponse(true));
    }
}
