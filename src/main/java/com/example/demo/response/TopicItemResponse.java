package com.example.demo.response;

import com.example.demo.entity.Topic;
import lombok.Data;

@Data
public class TopicItemResponse {
    private int id;
    private String name;
    private int order;
    private Topic topic;
}
