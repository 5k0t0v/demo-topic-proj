package com.example.demo.response;

import lombok.Data;

@Data
public class TopicResponse {
    private int id;
    private String name;
    private String description;
}
