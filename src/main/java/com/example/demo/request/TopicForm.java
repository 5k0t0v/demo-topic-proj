package com.example.demo.request;

import lombok.Data;

@Data
public class TopicForm {
    private String name;
    private String description;
    private String listTopicItems;
}
