package com.example.demo.request;

import lombok.Data;

@Data
public class TopicItemForm {
    private String name;
    private String order;
    private String topicId;
}
